job "metrics-nas" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  group "axion" {
    count = 1

    constraint {
      attribute = "${attr.cpu.arch}"
      value = "amd64"
    }

    network {
      mode = "bridge"
    }

    service {
      name = "axion-metrics"
      task = "telegraf"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "telegraf" {
      driver = "docker"

      vault {
        policies = ["snmp"]
      }

      resources {
        cpu = 250
        memory = 300
      }

      config {
        image = "[[ .metricsImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "snmp/axion" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN="{{ index .Data.data "TELEGRAF_TOKEN" }}"
          SNMP_HOST="{{ index .Data.data "SNMP_HOST" }}"
          SNMP_AUTH_USER="{{ index .Data.data "SNMP_AUTH_USER" }}"
          SNMP_AUTH_PROTOCOL="{{ index .Data.data "SNMP_AUTH_PROTOCOL" }}"
          SNMP_AUTH_PASS="{{ index .Data.data "SNMP_AUTH_PASS" }}"
          SNMP_ENCRYPTION_PROTOCOL="{{ index .Data.data "SNMP_ENCRYPTION_PROTOCOL" }}"
          SNMP_ENCRYPTION_PASS="{{ index .Data.data "SNMP_ENCRYPTION_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }
  }
}
